(defn función-<-1
[a, b]
(< a b))

(defn función-<-2
[a, b ]
(< (* 2 a) b))

(defn función-<-3
[a, b, c]
(< a b c))

(función-<-1 1 2) 
(función-<-2 3 4)
(función-<-3 4 5 6)


(defn función-<=-1
[a, b]
(<= a b))

(defn función-<=-2
[a, b ]
(<= (* 2 a) b))

(defn función-<=-3
[a, b, c]
(<= a b c))

(función-<=-1 1 2) 
(función-<=-2 3 4)
(función-<=-3 1/2 1/3 1/4)


(defn función-==-1
[a]
(== a))

(defn función-==-2
[a, b]
(== a, b)

(defn función-==-3
[a, b, c]
(== a, b, c))

(función->-1 1) 
(función->-2 3 4)
(función->-3 1/2 1/3 1/4)

(defn función->-1
[a, b]
(> a b))

(defn función->2
[a, b]
(> a, b))

(defn función->-3
[a, b, c, d, e]
(> a, b, c, d, e))

(función->-1 1 2)
(función->-2 2 1)
(función->-3 6 5 4 3 2)


(defn función->=-1
[a, b]
(>= a b))

(defn función->=-2
[a, b]
(>= a, b))

(función->=-1 2 1)
(función->=-2 2 2)
(función->=-3 6 5 4 3 2)

(defn función->=-3
[a, b, c, d, e]
(>= a, b, c, d, e))

(función->=-1 2 1)
(función->=-2 1/2 0.5)
(función->=-3 6 5 4 3 2)


(defn función-assoc-1
[a, b, c]
(assoc [a, b, c] 0 10 ))

(defn función-assoc-2
[a, b, c]
(assoc [a, b, c] 3 10 ))

(defn función-assoc-3
[a, b, c]
(asssoc [a, b, c] 4 10)

(función-assoc-1 1 2 3)
(función-assoc-2 1 2 3)
(función-assoc-2 1 2 3)


(defn funcion-assoc-in-1
  [dato]
  (assoc-in {:persona {:nombre "Diego"}} [:persona :nombre] dato))

(defn función-assoc-in-1
  [a]
  (assoc-in {:alumno {:nombre "Fernando"}} [:alumno :nombre] a))

(defn función-assoc-in-2
  [a b]
  (assoc-in {:alumno {:nombre "Fernando", :apellido "Sanchez"}}
            [:alumno :nombre] a)
  (assoc-in {:alumno {:nombre "Fernando", :apellido "Sanchez"}}
            [:alumno :apellido] b))

(defn función-assoc-in-3
  [ a ]
  (assoc-in {:objeto {:nombre "mesa", :color "negro"}}
            [:objeto :color ] a))

(funcion-assoc-in-1 "Chris")
(función-assoc-in-1 "Luis")
(función-assoc-in-2 "Luis" "Rodriguez")
(función-assoc-in-3 "Rojo")



(defn función-concat-1
[a b c d]
(concat [a b] [c d] ))

(defn función-concat-2
[a b c d e]
(concat [a b] [c] [d e] ))

(defn función-concat-3
[a b c]
(concat {:a a :b b} {:c c))

(función-concat-1 1 2 3 4)
(función-concat-2 1 2 3 4 5)
(función-concat-3 1 2 3)


(defn función-conj-1
[a b c]
(conj [a b] c))

(defn función-conj-2
[a b c]
(conj '(a b) c))

(defn función-conj-3
[a b c d]
(conj [a b c] d))



(función-conj-1 1 2 3)
(función-conj-2 1 2 3)
(función-conj-3 "a" "b" "c" "d")


(defn función-cons-1
[a b c]
(cons a[b c]))

(defn función-cons-2
[a b c d]
(cons [a b] [c d]))

(defn función-cons-3
[a b c]
(cons {:a a :b b} {:b c}))

(función-cons-1 3 2 1)
(función-cons-2 1 2 3 4)
(función-cons-3 1 2 3)


(defn función-contains?-1
[a]
(contains? {:a a} :a))

(defn función-contains?-2
[a]
(contains? [1 3 4] a))

(defn función-contains?-3
[a]
(contains? "f" a))

(función-contains?-1 :a)
(función-contains?-2 2)
(función-contains?-3 0)


(defn función-count-1
[a b c]
(count [a b c]))

(defn función-count-2
[a b]
(count {:uno a :dos b}))

(defn función-count-3
[a]
(count []))

(función-count-1 1 2 3)
(función-count-2 1 2)
(función-count-3 2)


(defn función-disj-1 
[a b c d]
(disj #{a b c} d))

(defn función-disj-2
[a b c d]
(disj #{a b c} d))

(defn función-disj-3
[a b c d e]
(disj #{a b c} d e))

(función-disj-1 1 2 3 2)
(función-disj-2 1 2 3 4)
(función-disj-3 1 2 3 1 3)


(defn función-disj-1
[a b c]
(dissoc {:a a :b b :c c})

(defn función-disj-2
[a b c]
(dissoc {:a a :b b :c c} :d)

(defn función-disj-3
[a b c]
(dissoc {:a a :b b :c c} :b b))


(función-disj-1 1 2 3)
(función-disj-2 1 2 3)
(función-disj-3 1 2 3)

10 
(defn función-distinct-1
[a b c d e f g h]
(distinct [a b c d e f g h]))

(defn función-distinct-1
[a b c d e f g h]
(distinct [a b c d e f g h]))

(defn funcion-distinct-2
  [a]
  (distinct a))

(defn funcion-distinct-3
[a]
(distinct a))


(función-distinct-1 1 2 1 3 1 4 1 5)
(función-distinct-2 "a" "a" "a" "a" "a" "a" "a" "b")
(funcion-distinct-2 '(1 2 3 4 5 1 2 3))

16.
(defn funcion-distinct?-1
[a b c]
(distinct? a b c))

(defn funcion-distinct?-2
[a b c d]
(distinct? a b c d))

(defn funcion-distinct?-3
[a b c d]
(distinct? a b c d))

(funcion-distinct?-1 1 2 3)
(funcion-distinct?-2 1 2 3 3)
(funcion-distinct?-3 1 2 3 1)

17. 
(defn funcion-drop-last-1
[a b c d]
(drop-last [a b c d]))

(defn funcion-drop-last-2
[a b c d e]
(drop-last a [b c d e]))

(defn funcion-drop-last-3
[a b c d e]
(drop-last a [b c d e]))

(funcion-drop-last-1 1 2 3 4)
(funcion-drop-last-2 -1 1 2 3 4)
(funcion-drop-last-3 5 1 2 3 4)

(defn funcion-drop-last-3
[a b c d e]
(drop-last a [b c d e]))

(ns clojure00.core)

(defn función-quot-1
  [a b]
  (quot a b))

(defn función-quot-2
  [a b]
  (= (/ a b)
     (quot a b)))

(defn función-quot-3
  [a b]
  (if (= (/ 4 2) (quot 4 2)) (+ a b) (* a b)))

(función-quot-1 10 3)
(función-quot-2 4 2)
(función-quot-3 4 2)

(defn función-range-1
  [a]
  (range a))

(defn función-range-2
  [a b]
  (range a b))

(defn función-range-3
  [a b c]
  (range a b c))

(función-range-1 10)
(función-range-2 -5 5)
(función-range-3 -100 100 10)

(defn función-rem-1
  [a b]
  (rem a b))

(defn función-rem-2
  [a b c d]
  (+ (rem a b)
     (rem c d)))

(defn función-rem-3
  [a b c d]
  (list (rem a b)
        (rem c d)))

(función-rem-1 10 9)
(función-rem-2 10 9 20 6)
(función-rem-3 10 9 20 6)

(defn función-repeat-1
  [a]
  (repeat a [1 2 3]))

(defn función-repeat-2
  [a b]
  (repeat a b))

(defn función-repeat-3
  [a b c d]
  (list (repeat a b)
        (repeat c d)))

(función-repeat-1 4)
(función-repeat-2 3 "PLF")
(función-repeat-3 2 "hola" 3 "PLF")

(defn función-replace-1
  [a]
  (replace [1 2 3 4 5] a))

(defn función-replace-2
  [a b]
  (replace a b))

(defn función-replace-3
  [a b c d]
  (list (replace a b)
        (replace c d)))


(función-replace-1 [0 2])
(función-replace-2 [10 9 8 7 6] [0 2 4])
(función-replace-3 [10 9 8 7 6] [0 2 4] [\a \b \c \d \e] [0 2 4])

(defn función-rest-1
  [a]
  (rest a))

(defn función-rest-2
  [a b]
  (vector (rest a)
          (rest b)))

(defn función-rest-3
  [a b]
  (apply str (rest a)
         (rest b)))

(función-rest-1 [1 2 3 4 5])
(función-rest-2 [1 2 3 4 5] ["a" "b" "c" "d"])
(función-rest-3 [10 20 30 40 50] [\A \B \C \D])

(defn función-select-keys-1
  [a b]
  (select-keys a b))

(defn función-select-keys-2
  [a b]
  (list (select-keys {:a 1 :b 2 :c 3} a)
        (select-keys {:a \A :b \B :c \C} b)))

(defn función-select-keys-3
  [a b]
  (apply str (list (select-keys {:a 1 :b 2 :c 3} a)
                   (select-keys {:a \A :b \B :c \C} b))))


(función-select-keys-1 {:a 1 :b 2} [:a])
(función-select-keys-2 [:a] [:b])
(función-select-keys-3 [:a] [:a])

(defn función-shuffle-1
  [a]
  (shuffle a))

(defn función-shuffle-2
  [a]
  (shuffle a))

(defn función-shuffle-3
  [a b]
  (repeatedly a (partial shuffle b)))

(función-shuffle-1 '(1 2 3))
(función-shuffle-2 [2 4 6])
(función-shuffle-3 3 [1 3 4])

(defn función-sort-1
  [a]
  (sort a))

(defn función-sort-2
  [a b]
  (split-at a (sort b)))

(defn función-sort-3
  [a b]
  (list (sort a)
        (sort b)))

(función-sort-1 [7 2 3 1])
(función-sort-2 3 [1 5 2 4 6 3])
(función-sort-3 [7 2 3 1] [1 5 2 4 6 3])

(defn función-split-at-1
  [a]
  (split-at a [1 2 3 4 5 6]))

(defn función-split-at-2
  [a]
  (str (split-at a [10 20 30 40 50 60])))

(defn función-split-at-3
  [a b]
  (vector (split-at a [1 2 3 4 5])
          (split-at b [10 20 30 40 50])))

(función-split-at-1 3)
(función-split-at-2 3)
(función-split-at-3 3 3)

(defn función-str-1
  [a]
  (str a))

(defn función-str-2
  [a]
  (apply str a))

(defn función-str-3
  [a b]
  (str a b))

(función-str-1 "hola")
(función-str-2 [1 2 3])
(función-str-3 \@ "twitter")


(defn función-subs-1
  [a]
  (subs "ejemplo" a))

(defn función-subs-2
  [a b]
  (subs "Ejemplo-2" a b))

(defn función-subs-3
  [a b, c, d]
  (list (subs "Ejemplo-3" a b)
        (subs "Cadenita" c d)))

(función-subs-1 3)
(función-subs-2 2 6)
(función-subs-3 0 9 6 8)


(defn función-subvec-1
  [a]
  (subvec [1 2 3 4 5 6 7 8] a))

(defn función-subvec-2
  [a b]
  (subvec [1 2 3 4 5 6 7 8] a b))

(defn función-subvec-3
  [a b c]
  (list (subvec [1 2 3 4 5] a)
        (subvec [10 20 30 40 50 60] b c)))

(función-subvec-1 4)
(función-subvec-2 2 6)
(función-subvec-3 3 2 4)


(defn función-take-1
  [a]
  (take a [10 20 30 40]))

(defn función-take-2
  [a b]
  (list (take a [10 20 30 40])
        (take b [10 20 30 40])))

(defn función-take-3
  [a b c]
  (vector (take a [10 20 30])
          (take b [40 50 60])
          (take c [70 80 90])))

(función-take-1 1)
(función-take-2 1 2)
(función-take-3 1 2 3)


(defn función-true?-1
  [a]
  (true? a))

(defn función-true?-2
  [a b]
  (true? (= a b)))

(defn función-true?-3
  [a b]
  (true? (> a b)))

(función-true?-1 4)
(función-true?-2 4 4)
(función-true?-3 4 5)


(defn función-val-1
  [a]
  val {:a a})

(defn función-val-2
  [a b]
  val {:a a  :b b})

(defn función-val-3
  [a b c]
  val {:a a  :b b :c c})

(función-val-1 "prueba")
(función-val-2 "uno" "dos")
(función-val-3 "uno" "dos" "tres")


(defn función-vals-1
  [a]
  (vals {:a a}))

(defn función-vals-2
  [a b]
  (vals {:a a  :b b}))

(defn función-vals-3
  [a b c]
  (vals {:a a  :b b :c c}))


(función-vals-1 "hola")
(función-vals-2 "hola" "mundo")
(función-vals-3 "Cristofher" "Diego" "Jimenez")


(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [a]
  (if (zero? a) ["Es un cero"] ["No es cero"]))

(defn función-zero?-3
  [a b]
  (vector (if (zero? a) ["Es un cero"] ["No es cero"])
          (if (zero? b) ["Es un cero"] ["No es cero"])))

(función-zero?-1 2)
(función-zero?-2 0)
(función-zero?-3 4 0)


(defn función-zipa
  [a]
  (zipmap [:a] [a]))

(defn función-zipmap-2
  [a b]
  (zipmap [:a :b] [a b]))

(defn función-zipmap-3
  [a b c]
  (zipmap [:a :b :c] [a b c]))

(función-zipa 2)
(función-zipmap-2 1 2)
(función-zipmap-3 1 2 3)  



 





